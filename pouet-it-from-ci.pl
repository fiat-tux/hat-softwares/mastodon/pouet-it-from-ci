#!/usr/bin/perl
use strict;
use warnings;
use 5.10.0;

use Mastodon::Client;

die 'No host defined. Aborting' unless $ENV{HOST};
die 'No message. Aborting.' unless $ENV{MESSAGE};

my $client = Mastodon::Client->new(
    instance        => $ENV{HOST},
    name            => 'Pouet it from CI',
    website         => 'https://framagit.org/fiat-tux/hat-softwares/mastodon/pouet-it-from-ci',
    scopes          => ['write'],
    client_id       => $ENV{CLIENT_ID},
    client_secret   => $ENV{CLIENT_SECRET},
    access_token    => $ENV{ACCESS_TOKEN},
    coerce_entities => 0
);

say 'Posting status';
$client->post_status($ENV{MESSAGE});

say 'All has been well. Message tooted.';
