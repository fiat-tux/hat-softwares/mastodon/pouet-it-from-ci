# Pouet it from CI

This software is used for automatically tooting from Gitlab CI when creating a new tag.

## How to use?

First, get credentials for your app with the [register-app](https://framagit.org/fiat-tux/hat-softwares/mastodon/register-app) script.

With those credentials, in the CI settings of your project, create the following protected variables:
- `CLIENT_ID`
- `CLIENT_SECRET`
- `ACCESS_TOKEN`
- and create a `HOST` protected variable, containing the hostname of your Mastodon instance.

Then protect all your tags in the repository settings by creating a `*` wildcard.

Finally, add this to your `.gitlab-ci.yml`:
```
pouet-it:
  image: hatsoftwares/pouet-it-from-ci:latest
  script:
    - cd /opt/pouet-it-from-ci
    # If you want to uppercase the first letter of your project, uncomment following lines and use ${project} instead of ${CI_PROJECT_NAME}
    # in the export MESSAGE line
    #- export project="$(tr '[:lower:]' '[:upper:]' <<< ${CI_PROJECT_NAME:0:1})${CI_PROJECT_NAME:1}"
    # If you have dashes in the name of your project, you can use the following line to remove them and uppercase the first letter of each word
    #- export project="$(echo $CI_PROJECT_NAME | perl -p -e 's/(?:^(.)|-(.))/\U$1\U$2/g')"
    - export MESSAGE=$(echo -e "I just released a new version of "'#'"${CI_PROJECT_NAME}!""\n${CI_PROJECT_URL}/tags/${CI_COMMIT_TAG}")
    - carton exec ./pouet-it-from-ci.pl
  only:
    - tags
```

And that should be all.

## License

GPLv3, see the [LICENSE](LICENSE) file for details.

## Logo

The logo is the [mascot of Mastodon](https://commons.wikimedia.org/wiki/File%3AMastodon_Mascot_%28Alternative%29.png)

## Author

[Luc Didry](https://fiat-tux.fr). You can support me on [Tipeee](https://tipeee.com/fiat-tux) and [Liberapay](https://liberapay.com/sky).

[![Tipeee button](https://framagit.org/luc/last/raw/master/themes/default/img/tipeee-tip-btn.png)](https://tipeee.com/fiat-tux) [![Liberapay logo](https://framagit.org/luc/last/raw/master/themes/default/img/liberapay.png)](https://liberapay.com/sky)
